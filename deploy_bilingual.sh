#!/bin/sh
# usage ./deploy_bilingual.sh nomsite idsite
#site=oasis
site=$1
siteid=$2
wordpress=/var/www/html/wordpress
DIR="$( cd "$( dirname "$0" )" && pwd )"
cd $wordpress
rm -rf wp-content/uploads/sites/$siteid/wp2static-processed-site
rm -rf wp-content/uploads/sites/$siteid/wp2static-crawled-site
/usr/bin/php $DIR/change_language.php $siteid en
/usr/local/bin/wp --allow-root --url=http://make-wp.inrialpes.fr/wordpress/$site/ wp2static full_workflow
rm -rf wp-content/uploads/sites/$siteid/wp2static-crawled-site
mv wp-content/uploads/sites/$siteid/wp2static-processed-site wp-content/uploads/sites/$site.en
find wp-content/uploads/sites/$site.en -type f -name index.html -print0 | xargs -0 sed -i "s# href=\"/# href=\"/en/#g"
find wp-content/uploads/sites/$site.en -type f -name index.html -print0 | xargs -0 sed -i "s# href=\"/en/fr/# href=\"/#g"
/usr/bin/php $DIR/change_language.php $siteid fr
/usr/local/bin/wp --allow-root --url=http://make-wp.inrialpes.fr/wordpress/$site/ wp2static full_workflow
mv wp-content/uploads/sites/$site.en wp-content/uploads/sites/$siteid/wp2static-processed-site/en
cd wp-content/uploads/sites/$siteid/wp2static-processed-site
find . -type d -exec curl -s --netrc-file /root/.netrc -X MKCOL "https://files.inria.fr:8443/$site/{}" \;
find . -type f -exec curl -s --netrc-file /root/.netrc -T {} "https://files.inria.fr:8443/$site/{}" \;
