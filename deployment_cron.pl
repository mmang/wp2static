#!/usr/bin/perl -w

use strict;
use warnings;
use File::Basename;
my $dirname = dirname(__FILE__);

#my $site = 'oasis';
my $site = $ARGV[0];
my $siteoption = $ARGV[1] || '';
my $siteid = `/usr/bin/php $dirname/get_blog_id.php $site`;
# URL à localiser dans les logs
#my @adminurl = ('wordpress/oasis/wp-admin','wordpress/steep/wp-admin');
my $adminurl = 'wordpress/'.$site.'/wp-admin';
my $logdir = '/var/log/httpd';
my $logfilename = 'access_log';
# durée de la session en minutes
#my $session = 24;
my $session = 10;
my $logfile = $dirname . '/deployment_'.$site.'.log';

my %months = ('Jan' => 1, 'Feb' => 2, 'Mar' => 3, 'Apr'=>4, 'May'=>5, 'Jun'=>6,
       	'Jul'=>7, 'Aug'=>8, 'Sep'=>9, 'Oct'=>10, 'Nov'=>11, 'Dec'=>12 );
our ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst);
our ($lsec,$lmin,$lhour,$lday,$lmonth,$lyear);
my $lignelog = 'ligne';
my $ligne = '';
if (-e $logfile) {
	open(LOG,'<',$logfile); 
	$lignelog = <LOG>;
	close(LOG);
}

while (1) {
  ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();
  $mon++;
  $year += 1900;
  $ligne = `grep '$adminurl' $logdir/$logfilename | tail -1`;
  unless ($ligne =~ /^[^\[]+\[(\d\d)\/([A-Z][a-z][a-z])\/(\d\d\d\d):(\d\d):(\d\d):(\d\d) .*$/) {
    my $secondlogfile = `ls -1 $logdir | grep -E '^$logfilename-[0-9]+\$'`;
    chomp $secondlogfile;
    $ligne = `grep '$adminurl' $logdir/$secondlogfile | tail -1`;
  }
  if ($ligne =~ /^[^\[]+\[(\d\d)\/([A-Z][a-z][a-z])\/(\d\d\d\d):(\d\d):(\d\d):(\d\d) .*$/) {
    $lyear = $3;
    $lmonth = $2;
    $lday = $1;
    $lhour = $4;
    $lmin = $5;
    $lsec = $6; 
    if ($ligne eq $lignelog) {
      die "$mday/$mon/$year $hour:$min:$sec Déploiement déjà effectué !\n";
    }
  }
  else {
    die "Format de log incorrect: $ligne\n";
  }

  $lmonth = $months{$lmonth};
  print STDERR "$mday/$mon/$year $lhour:$lmin:$lsec || $hour:$min:$sec\n";
  if ($year-$lyear==1) {
	  $year-=1;
	  $mon+=1;
  }
  if ($lyear == $year) {
    if ($mon-$lmonth==1) {
      $mday +=1;
      $mon =-1;
    }
    if ($lmonth == $mon) {
      if ($mday-$lday==1) {
        $hour+=24;
	$mday-=1;
      }
      if ($lday == $mday) {
        if ($hour-$lhour==1) {
          $min += 60;
	  $hour -=1;
        }
        if ($hour == $lhour) {
	  if ($min-$lmin<$session) {
            print STDERR 'sleep: ',($session - $min + $lmin)*60,"\n";
            sleep(($session - $min + $lmin)*60);
	  }
	  else {
   	    last;
	  }
        }
        else {
	  last;
        }
      } 
      else {
        last;
      }
    }
    else {
      last;
    }
  }
}

print STDERR "$mday/$mon/$year $lhour:$lmin:$lsec || $hour:$min:$sec\n";
open(LOG, '>', $logfile) or die "File is not opening '$logfile' $!";
print LOG $ligne;
close LOG;
print STDERR "$mday/$mon/$year $hour:$min:$sec\tDeploy $site\n";
`$dirname/deploy$siteoption.sh $site $siteid`;
($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();
$mon++;
$year += 1900;
print STDERR "$mday/$mon/$year $hour:$min:$sec\tEnd deploy $site\n";
